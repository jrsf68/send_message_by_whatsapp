from app import __version__
from app import send_message_by_whatsapp as app


def test_version():
    assert __version__ == '0.1.0'


def test_read_file_messages():
    messages_filename = "messages_whatsapp.csv"

    handler_messages_filename = app.read_file_messages(messages_filename)
    
    assert handler_messages_filename is not None


def test_read_file_messages_erro():
    messages_filename = "this_file_not_exists"
    
    handler_messages_filename = app.read_file_messages(messages_filename)
    
    assert handler_messages_filename is None


def test_open_whatsapp_webpage():
    handler_browser = app.open_whatsapp_webpage()
    assert handler_browser is not None
    

# def test_get_mobile_number():
#     messages_filename = "messages_whatsapp.csv"
#     message_index = 0
#     test_mobile_number = "5591981277200"
#     handler_messages_filename = app.read_file_messages(messages_filename)
    
#     mobile_number = app.get_mobile_number(handler_messages_filename, message_index)
    
#     assert mobile_number == test_mobile_number
     

# def test_get_message_text():
#     messages_filename = "messages_whatsapp.csv"
#     message_index = 0
#     test_message_text = "Ol%C3%A1%2C%20Ricardo%21%0A%0AMensagem%20teste%20via%20Whatsapp"
#     handler_messages_filename = app.read_file_messages(messages_filename)

#     message_text = app.get_message_text(handler_messages_filename, message_index)
    
#     assert message_text == test_message_text


# # def test_send_message():
# #     messages_filename = "messages_whatsapp.csv"
# #     message_index = 0
# #     handler_messages_filename = app.read_file_messages(messages_filename)
# #     mobile_number = app.get_mobile_number(handler_messages_filename, message_index)
# #     message_text = app.get_message_text(handler_messages_filename, message_index)

# #     result_send_message = app.send_message(handler_messages_filename, mobile_number, message_text)

# #     assert result_send_message


# def test_whatsapp_ready_to_send():
#     component_not_present_on_page = '<span class="selectable-text copyable-text" data-lexical-text="true">'
#     handler_browser = app.open_whatsapp_webpage()

#     page_is_ready = app.whatsapp_ready_to_send(handler_browser, component_not_present_on_page)
    
#     assert page_is_ready