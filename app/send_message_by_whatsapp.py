"""Send message by Whatsapp

   To automate the sending of messages by whatsapp you need:
    Input: text file messages
    Processing: Read file messages
                # Open Whatsapp web page, authenticate user account with QR Code and wait to display number text box
                Get mobile number
                Get message text
                # Find by mobile number textbox xpath , input mobile number, press enter end wait to display message text box
                # Find by textbox xpath, input message, press enter end wait to display number text box
                Close Whatsapp web page
                Close file messages
    Output: Send messages              
"""


from selenium import webdriver
from selenium.webdriver.common.by import By
import urllib.parse
import pandas as pd
import time
import re

def read_file_messages(messages_filename: str) -> pd.core.frame.DataFrame:
    """ Read file message  to send. End of line is marked with "|" character. this allows "enter" in the message text.

    Args:
        messages_filename (str): .csv file name written to disk.

    Returns:
        pd.core.frame.DataFrame: pandas dataframe containing the messages read from the file.
    """

    try:
        dataframe_message_data = pd.read_csv(messages_filename, sep=';', lineterminator='|', header=[0])
    except OSError as e:
        print(e)
        return None

    return dataframe_message_data


def open_whatsapp_webpage():
    timeout = 0
    browser_driver = webdriver.Chrome()
    browser_driver.get("https://web.whatsapp.com/")
    while len(browser_driver.find_elements(By.ID, 'pane-side')) < 1:
        time.sleep(3)
        timeout += 3
        if timeout > 30:
            return 0
    return browser_driver


def get_mobile_number(handler_messages_filename, message_index):
    mobile_number_read = handler_messages_filename["contato"][message_index]
    mobile_number_without_enter = re.sub('\n', '', mobile_number_read)
    mobile_number = urllib.parse.quote(mobile_number_without_enter)
    return mobile_number


def get_message_text(handler_messages_filename, message_index):
    message_text_read = handler_messages_filename["mensagem"][message_index]
    message_text = urllib.parse.quote(message_text_read)
    return message_text


def whatsapp_ready_to_send(browser_driver, component_not_present_on_page):
    timeout = 0
    while len(browser_driver.find_elements(By.ID, component_not_present_on_page)) > 0:
        time.sleep(3)
        timeout += 3
        if timeout > 30:
            return False
    return True
